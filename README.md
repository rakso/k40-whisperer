# K40 Whisperer

Control software for the stock K40 Laser controller.

This repository on the `main` branch contains all source files from [this](https://www.scorchworks.com/K40whisperer/k40whisperer.html#download) site.

See https://www.scorchworks.com/K40whisperer/k40whisperer.html to learn more about this software and its creator.

## Getting started

- [Setup instructions for Linux](k40_whisperer/README_Linux.txt)
- [Setup instructions for MacOS](k40_whisperer/README_MacOS.md)

### Linux users info

If you ran into `ModuleNotFoundError: No module named 'tkinter'` error, try to install package `python3-tk`. [More info](https://stackoverflow.com/a/66292120/10717179) on Stack Overflow.

#### Small "getting started script" - make sure to have `Tkinter` module installed

```bash
python3 -m venv venv
source venv/bin/activate
cd k40_whisperer/
pip install -r requirements.txt
python ./k40_whisperer.py
```

## Changelog and license file

- [Changelog](k40_whisperer/Change_Log.txt)
- [GPL 3.0](k40_whisperer/gpl-3.0.txt) license
